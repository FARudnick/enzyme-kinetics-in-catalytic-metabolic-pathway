# Enzyme Kinetics in Catalytic Metabolic Pathway

A combination of four programs allows simulation of catalytic metabolic pathways with multiple steps. Each step may contain two enzymes (of one locus) which create a chemically indifferent product which was tracked for calculation purposes.

About operational file:
- has to contain in this order:
    1) number of catalytic metabolic pathway steps (no limit set in program, has been successfully tried with up to five steps)
    2) length of time series (default is 5000 ticks)
    3) substrate starting concentration (default is 0)
    4) initialization of rate constants (default is "s" -> all enzymes are initialized with default values for rate constant (kf = 0.001, kr = kcat =  0.01))
    5) substrate flux per tick (default is 0.2)
    6 - ...) enzyme starting concentrations; two for each step (e.g. 2 steps -> two enzymes for first step, two for the second)

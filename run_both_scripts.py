import sys


what_parameter = input("What parameter (kf, kr, kcat, sflux)? ")
what_enzyme = (input("Which enzyme should be manipulated (0, ..., step - 1)? ") or "0")
if what_parameter == "sflux":
    enzyme_1 = input("Enzyme 1 starting concentration: ")
    enzyme_2 = input("Enzyme 2 starting concentration: ")
parameter_variable = 0.0
# substrate_flux = 0.1

with open("what_parameter.txt", "w") as file:
    file.write(what_parameter + "\n")
    file.write(what_enzyme)

if what_parameter == "kf":
    parameter_variable = 0.001  # 0.001
# elif what_parameter == "kr" or what_parameter == "kcat":
#     parameter_variable = 0.01
elif what_parameter == "kr":
    parameter_variable = 0.01
elif what_parameter == "kcat":
    parameter_variable = 0.01
elif what_parameter == "sflux":
    parameter_variable = 0.1
else:
    sys.exit("Error: wrong input")
# kr = 0.01
# kcat = 0.01
for i in range(20):
    if what_parameter == "sflux":
        with open("enzyme_dominance_operationalFile.txt", "w") as file:
            file.write("2\n")
            file.write("5000\n")
            file.write("0\n")
            file.write("s\n")
            file.write(str(round(parameter_variable, 1)) + "\n")
            file.write(enzyme_1 + "\n")
            file.write(enzyme_2 + "\n")
            file.write("85\n")
            file.write("15")
    else:
        with open("parameter_temp.txt", "w") as file:
            if what_parameter == "kf":
                file.write(str(round(parameter_variable, 3)))
            else:
                file.write(str(round(parameter_variable, 2)))
    exec(open("enzyme_dominance.py").read())
    exec(open("natural_selection.py").read())
    if what_parameter == "sflux":
        parameter_variable += 0.1
    else:
        if what_parameter == "kf":
            parameter_variable += 0.001  # 0.001
        else:
            parameter_variable += 0.01
    # kr += 0.01
    # kcat += 0.01
    # substrate_flux += 0.1

from datetime import date
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt
import time
import os
import json


def parameters(n, mode, sflux):
    parms = []
    scount = int(sum([2 ** m for m in range(n)]))
    pcount = int(scount + 2 ** n - 1)  # first substrate i.e. product of 0th step needs to be cut
    # mode = input("Should parameters be set to standard (enter: s) or individually (enter: c)? ")
    # sflux = float(input("Enter substrate fluctuation: "))
    parameter_change = 0
    parameter_file_exists = os.path.isfile("what_parameter.txt")
    # print(parameter_file_exists)
    what_parameter = ""
    what_enzyme = 0
    parameter_variable = ""

    # simple mode creates vectors for enzymes with standard parameters
    if mode == "s":
        # standard values for enzyme kinetics canonical enzyme reaction
        kf = 0.001  # standard: 0.001
        kr = 0.01  # standard: 0.01
        kcat = 0.01

        if parameter_file_exists:
            with open("what_parameter.txt", "r") as file:
                what_parameter = file.readline().strip()
                what_enzyme = int(file.readline())
                print(what_parameter + " -> " + str(what_enzyme))
            if what_parameter != "sflux":
                with open("parameter_temp.txt", "r") as file:
                    # kf_parameter = file.readline()
                    # kr_parameter = file.readline()
                    # kcat_parameter = file.readline()
                    parameter_variable = file.readline()

        # fill parameter vector
        for i in range(2 * n + 1):
            # values for enzymes
            if i < 2 * n:
                # for parameter comparison
                # change i value between 0 and 1 to access correct enzyme to change
                if parameter_file_exists and what_parameter != "sflux":
                    if i == what_enzyme:
                        # kf = float(kf_parameter)
                        # kr = float(kr_parameter)
                        # kcat = float(kcat_parameter)
                        if what_parameter == "kf":
                            kf = float(parameter_variable)
                            parameter_change = kf
                        elif what_parameter == "kr":
                            kr = float(parameter_variable)
                            parameter_change = kr
                        elif what_parameter == "kcat":
                            kcat = float(parameter_variable)
                            parameter_change = kcat
                        # parameter_change = kf
                    else:
                        if what_parameter == "kf":
                            kf = 0.001  # 0.001
                        elif what_parameter == "kr":
                            kr = 0.01  # 0.01
                        elif what_parameter == "kcat":
                            kcat = 0.01
                        # kf = 0.001
                        # kr = 0.01
                        # kcat = 0.01
                parms.append([kf, kr, kcat])
            # append information about substrate flux (sflux), number of substrates (scount) and products (pcount)
            else:
                parms.append([sflux, scount, pcount])
    # complex mode allows user to choose individual parameters for each enzyme
    elif mode == "c":
        for i in range(2 * n + 1):
            if i < 2 * n:
                kf = float(input("Enter kf-value for enzyme " + str(i + 1) + ": "))
                kr = float(input("Enter kr-value for enzyme " + str(i + 1) + ": "))
                kcat = float(input("Enter kcat-value for enzyme " + str(i + 1) + ": "))
                parms.append([kf, kr, kcat])
            # additional information as aforementioned
            else:
                parms.append([sflux, scount, pcount])
    else:
        print("Wrong input type (should be s or c).")
    print("parameters: " + str(parms))

    with open("parameter_change.txt", "a") as file:
        file.write(str(parameter_change) + "\n")

    return parms


def zuperZygous(t, z, parms):
    # z structure: z = (e_1,...,e_n, s_1,...,s_(2^(n-1)), c_1,...,c_k, p_1,...p_k)
    n = len(parms) - 1
    scount = parms[n][1]
    pcount = parms[n][2]
    sflux = parms[n][0]
    v = [0] * len(z)
    k = -1
    o = 0

    # structure of ordinary differential equation (ODE)
    # based on four ODEs behind Michaelis-Menten kinetics
    for i in range(n):
        kf = parms[i][0]
        kr = parms[i][1]
        kcat = parms[i][2]
        e = z[i]

        if i % 2 == 0:
            k += 1

        # loop over substrates
        for j in range(n - 1 + 2 ** k, n - 1 + 2 ** (k + 1)):
            s = z[j]
            c = z[n + scount + o]
            v[i] += -kf * e * s + kr * c + kcat * c
            v[j] += -kf * e * s + kr * c
            v[n + scount + o] += kf * e * s - kr * c - kcat * c
            p = kcat * c
            v[n + scount + pcount + o] += p
            o += 1
            if k < (int(n / 2) - 1):
                v[n + o] += p

        v[n] += sflux

    return v


def dominance(pUnited, n):
    cazor = []
    temp = []
    fitbit = []
    fitness = []
    stepCounter = n

    # create temporary file whose stored information is used again in different program part
    for i in range(3 ** n):
        temp.append([])

    # calculate dominance values with information on generated products by enzymes
    for i in range(n):
        if (i + 1) < n:
            k = 1
        else:
            k = 3
        x = 1
        y = 0
        c = 0
        dom = []
        counter = 1
        # loop over combinations of cases between steps
        for j in range(3 ** n):
            v = 3 ** (n - 1 - i)
            # index to choose combination
            index = (y * k) + (x - 1) * v
            # print(index)
            # get products from step 'i' from combination 'index'
            runner = pUnited[index][i]
            dom.append(runner)
            temp[j].append(c)
            # for fitness get product information from last step for each combination
            if i < (n - 1):
                runner = pUnited[index][n - 1]
                fitbit.append(runner)
            if (j + 1) % v == 0:
                c += 1
                if c == 3:
                    c = 0

            # calculate dominance via relation of product information of a homozygous case to the heterozygous one
            # omitting the other homozygous case
            if x == 3:
                # structure of dominance calculation:
                # dominance(enzyme1) = heterozygous - homozygous(enzyme2) / homozygous(enzyme1) - homozygous(enzyme2)
                domEnzymeOne = list(map(lambda a, b, r: (round(a, 10) - round(r, 10)) / (round(b, 10) - round(r, 10)) if round(b, 10) != round(r, 10) else 0.5, dom[0], dom[1], dom[2]))
                # domEnzymeTwo = list(map(lambda a, b, r: (a - b) / (r - b), dom[0], dom[1], dom[2]))
                domEnzymeTwo = [1 - value for value in domEnzymeOne]
                # put dominance values for both enzymes of a step together
                cazor.append([domEnzymeOne, domEnzymeTwo])
                if i == (n - 1):
                    fitness.append([domEnzymeOne, domEnzymeTwo])
                # calculate dominance for fitness
                if i < (n - 1):
                    domLastStepOne = list(map(lambda a, b, r: (round(a, 10) - round(r, 10)) / (round(b, 10) - round(r, 10)) if round(b, 10) != round(r, 10) else 0.5, fitbit[0], fitbit[1], fitbit[2]))
                    # domLastStepTwo = list(map(lambda a, b, r: (a - b) / (r - b), fitbit[0], fitbit[1], fitbit[2]))
                    domLastStepTwo = [1 - value for value in domLastStepOne]
                    fitness.append([domLastStepOne, domLastStepTwo])
                dom = []
                fitbit = []
                y += 1
                if 0 != i and i != (n - 1) and (j + 1) % 3**(n - i) == 0:
                    y = counter * 3**stepCounter
                    counter += 1
                x = 1
            else:
                x += 1
        stepCounter -= 1

    # create documentation files
    with open("fitness_values_temp.txt", "w") as file:
        json.dump(fitness, file)

    with open("temp.txt", "w") as file:
        file.write(str(temp))

    with open("tempAnalyzer.txt", "w") as file:
        counter = 0
        for line in temp:
            file.write(str(counter) + ": " + str(line) + "\n")
            counter += 1

    return cazor


def inititalState(n, parms, substrate, mo):
    scount = parms[2 * n][1]
    pcount = parms[2 * n][2]
    result = 3 ** n * [0]
    caseEnzyme = []  # 2 enzymes per step * 3 cases per enzyme
    combiEnzyme = []
    itVector = [-1] * n

    # create basic combinations for loci
    # can be either heterozygous or homozygous for either enzyme
    for i in range(n):
        # get enzyme concentrations
        e1 = float(mo[5 + 2 * i])
        e2 = float(mo[6 + 2 * i])
        het = [e1, e2]
        # homozygous case is double the enzyme concentration than its heterozygous concentration
        hom1 = [2 * e1, 0]
        hom2 = [0, 2 * e2]
        caseEnzyme.append([het, hom1, hom2])
    print("caseEnzyme: " + str(caseEnzyme))

    # genotype to be used in another program
    genotype_vector = []

    # calculate combinations of cases
    for i in range(3 ** n):
        genotype = ""
        combination = []
        k = n
        for j in range(n):
            k -= 1
            if i % 3 ** k == 0:
                itVector[j] += 1
                if itVector[j] == 3:
                    itVector[j] = 0
            z = caseEnzyme[j][itVector[j]]
            combination.append(z)
            # genotype is stored as string vector
            # '0' means a dominant allele
            # '1' a recessive allele
            if itVector[j] == 0:
                # heterozygous dominant
                genotype += "01"
            elif itVector[j] == 1:
                # homozygous dominant
                genotype += "00"
            elif itVector[j] == 2:
                # homozygous recessive
                genotype += "11"
        combiEnzyme.append(combination)
        genotype_vector.append(genotype)

    print("combiEnzyme: " + str(combiEnzyme))

    with open("genotype_vector.txt", "w") as file:
        json.dump(genotype_vector, file)

    # fill initial state vector
    for i in range(3 ** n):
        result[i] = [0] * (2 * n + scount + 2 * pcount)
        counter = 0
        for j in range(n):
            # get a case
            stepper = combiEnzyme[i][j]
            result[i][counter] = stepper[0]
            counter += 1
            result[i][counter] = stepper[1]
            counter += 1
        # add substrate information
        result[i][2 * n] = substrate

    # store initial state vector
    today = date.today()
    file = open("initialStates_" + str(today) + ".txt", "w")
    counter = 0
    for line in result:
        file.write(str(counter) + ": " + str(line) + "\n")
        counter += 1
    file.close()

    return result


def unite_products(n, k, length, cases):
    pUnited = []
    for i in range(3 ** n):
        pCase = []
        for j in range(n):
            pSum = [0] * length
            y = 2 ** (j + 1)
            for x in range(y):
                pSum = [sum(v) for v in zip(pSum, cases[k + x])]  # cases[k + x]
            k += y
            pCase.append(pSum)
        pUnited.append(pCase)

    return pUnited


def main():
    # method = input("Enter method (simple, complex, simpleMean, complexMean): ")  # at the moment no meaning but "no mean" ...
    # ... means one standard combination of cases is checked; mean goes over all combinations of cases and calculates
    # the mean and maybe a confidence value concerning deviation of cases
    cases = []
    substrateResults = []
    # structure mo = [steps, time, substrate, mode, substrate flux, enzyme1, enzyme2, ..., enzymeN]
    mo = [line.strip() for line in open("enzyme_dominance_operationalFile.txt", "r")]
    print("input information: " + str(mo))
    n = int(mo[0])  # count of metabolic steps
    time = float(mo[1])  # int(input("Enter time: "))
    substrate = float(mo[2])  # float(input("Enter substrate starting concentration: "))

    mode = str(mo[3])
    sflux = float(mo[4])
    # get parameters for ordinary differential equation (ODE) that solves Michaelis-Menten kinetics
    parms = parameters(n, mode, sflux)
    # get initial states (for different genotypes) for ODE
    init = inititalState(n, parms, substrate, mo)
    t = np.linspace(0, time)

    today = date.today()
    workdir = os.getcwd()

    # create directory where plots will be stored
    if not os.path.isdir(workdir + "\\plots"):
        os.mkdir(workdir + "\\plots")

    if not os.path.isdir(workdir + "\\plots\\ode_results"):
        os.mkdir(workdir + "\\plots\\ode_results")

    # create storing scaffold for plots
    fileExists = True
    counter = 0
    while fileExists:
        path = workdir + "\\plots\\ode_results\\metDom_ode_result_plots_" + str(today) + "_" + str(n) + "_" + str(counter) + ".pdf"
        fileExists = os.path.isfile(path)
        if fileExists:
            counter += 1

    with PdfPages(path) as export_pdf:

        # loop over cases/genotypes
        for count in range(3 ** n):
            # solve ODE
            sol = solve_ivp(zuperZygous, t_span=[0, time], y0=init[count], args=[parms],
                            dense_output=True)

            result = sol.sol(t)

            # loop over result to create plots for: free enzymes, substrate(s), complexes and products
            scount = parms[2 * n][1]
            pcount = parms[2 * n][2]
            k = 2 * n + scount + 2 * pcount

            # generate plots
            for j in range(int(k)):
                # free enzymes
                if j < 2 * n:
                    plt.plot(t, result[j].T, label=str(count) + "-e_index:" + str(j))
                # substrates
                elif j < (2 * n + scount):
                    if j == 2 * n:
                        plt.legend(shadow=True, ncol=3)
                        plt.grid()
                        export_pdf.savefig()
                        plt.close()
                    plt.plot(t, result[j].T, label=str(count) + "-s_index:" + str(j))
                    if j == 2 * n:
                        substrateResults.append(result[j])
                # complexes
                elif j < (2 * n + scount + pcount):
                    if j == 2 * n + scount:
                        plt.legend(shadow=True, ncol=3)
                        plt.grid()
                        export_pdf.savefig()
                        plt.close()
                    plt.plot(t, result[j].T, label=str(count) + "-c_index:" + str(j))
                # products
                elif j < (2 * n + scount + 2 * pcount):
                    if j == 2 * n + scount + pcount:
                        plt.legend(shadow=True, ncol=3)
                        plt.grid()
                        export_pdf.savefig()
                        plt.close()
                    plt.plot(t, result[j].T, label=str(count) + "-p_index:" + str(j))
                    cases.append(result[j])
            plt.legend(shadow=True, ncol=3)
            plt.grid()
            export_pdf.savefig()
            plt.close()

    # combine products for each step in each case/genotype
    k = 0
    # pUnited = []
    length = len(result[0])
    pUnited = unite_products(n, k, length, cases)

    # data structure for plots on parameter discussion (product concentration as information)
    with open("plot_data_products.txt", "a") as file:
        file.write(str(mo) + "\n")
        for case in pUnited:
            runner_step = []
            for i in range(n):
                runner = case[i][len(case[0]) - 1]
                runner_step.append(runner)
                # file.write(str(runner) + "\n")
            # print(runner_step)
            file.write(str(runner_step) + "\n")
        file.write("------\n")  # six times dash

    with open("product_results.txt", "w") as file:
        json.dump(pUnited, file)

    with open("product_results_clear.txt", "w") as file:
        counter = 1
        for i in pUnited:
            file.write(str(counter) + ":")
            for j in i:
                file.write("\t" + str(j) + "\n")
            counter += 1
            file.write("\n")

    # get dominance for enzymes
    domResult = dominance(pUnited, n)
    with open("domResult.txt", "w") as file:
        iterator = 0
        for line in domResult:
            file.write(str(iterator) + ": " + str(line) + "\n\n")
            iterator += 1

    if not os.path.isdir(workdir + "\\plots\\dominance_behaviour"):
        os.mkdir(workdir + "\\plots\\dominance_behaviour")

    # data structure for plots on parameter discussion (dominance behaviour)
    with open("plot_data_dominance.txt", "a") as file:
        file.write(str(mo) + "\n")
        for i in range(n * 3 ** (n - 1)):
            runner_step = []
            for j in range(2):
                runner = domResult[i][j]
                runner_step.append(runner[len(runner) - 1])
            file.write(str(runner_step) + "\n")
        file.write("------\n")

    # new plots
    fileExists = True
    counter = 0
    while fileExists == True:
        path = workdir + "\\plots\\dominance_behaviour\\metDom_ode_dominance_plots_" + str(today) + "_" + str(n) + "_" + str(counter) + ".pdf"
        fileExists = os.path.isfile(path)
        if fileExists == True:
            counter += 1

    # get genotype information to create meaningful plot titles
    cases = ["het", "hom(1)", "hom(1')"]
    with open("temp.txt", "r") as file:
        intel = json.load(file)
    os.remove(workdir + "\\temp.txt")

    with open("fitness_values_temp.txt", "r") as file:
        fitness = json.load(file)
    os.remove(workdir + "\\fitness_values_temp.txt")

    with PdfPages(path) as export_pdf:

        # put in information on experiment
        firstPage = plt.figure(figsize=(11.69, 8.27))
        firstPage.clf()
        txt = "Entered information: \n " + str(mo)
        firstPage.text(0.5, 0.5, txt, transform=firstPage.transFigure, size=8, ha="center", va="bottom")
        export_pdf.savefig()
        plt.close()

        k = 0
        y = 0
        c = n - 1
        # generate plots on dominance
        for i in range(n * 3**(n - 1)):
            plt.plot(t, domResult[i][0], label="enzyme 1")
            plt.plot(t, domResult[i][1], label="enzyme 2")
            # print(domResult[i][0])
            # print(domResult[i][1])
            plt.legend(shadow=True)
            plt.grid()
            titleAppend = ""
            index = y
            for x in range(n):
                if x != k:
                    choice = cases[int(intel[index][x])]
                    titleAppend += "[step " + str(x + 1) + " " + str(choice) + "] "
            if k <= (n - 1):
                y += 1

            if (i + 1) % 3**c == 0:
                if k == 0:
                    y += 3**c * k
                else:
                    y += 3**c * 2
            plt.title("Dominance Factors for Enzymes of Step " + str(k + 1) + "\n" + titleAppend, fontsize=8)
            plt.ylabel("dominance factor")
            plt.xlabel("time")
            export_pdf.savefig()
            plt.close()

            # plots on combined products
            newcount = 0
            for j in range(3):
                plt.plot(t, pUnited[index + newcount][k], label=str(cases[j]) + "_product(step: " + str(k + 1) + ")")
                newcount += 3 ** c
            plt.plot(t, substrateResults[index], label="substrate for \n first step")
            plt.legend(shadow=True)
            plt.title("Collective Products of Case" + "\n" + titleAppend, fontsize=8)
            plt.ylabel("product concentration")
            plt.xlabel("time")
            plt.grid()
            export_pdf.savefig()
            plt.close()

            # plots on product starting behaviour
            newcount = 0
            for j in range(3):
                plt.plot(t, pUnited[index + newcount][k], label=str(cases[j]) + "_product(step: " + str(k + 1) + ")")
                newcount += 3 ** c
            plt.plot(t, substrateResults[index], label="substrate for \n first step")
            plt.legend(shadow=True)
            plt.title("Collective Products of Case" + "\n" + titleAppend + " -- Starting Behaviour", fontsize=8)
            plt.ylabel("product concentration")
            plt.xlabel("time")
            plt.grid()
            plt.xlim(0, 500)
            plt.ylim(0, 1000)
            export_pdf.savefig()
            plt.close()

            # fitness
            if k < (n - 1):
                plt.plot(t, fitness[i][0], label="fitness enzyme 1")
                plt.plot(t, fitness[i][1], label="fitness enzyme 2")
                plt.legend(shadow=True)
                plt.title("Fitness in Last Step \n for " + titleAppend, fontsize=8)
                plt.ylabel("dominance value")
                plt.xlabel("time")
                plt.grid()
                export_pdf.savefig()
                plt.close()

            if (i + 1) % 3**(n - 1) == 0:
                k += 1
                y = 0
                c -= 1


if __name__ == "__main__":
    start_time = time.time()
    main()
    print("--- %s seconds ---" % (time.time() - start_time))

from math import log
import matplotlib.pyplot as plt
import json
import time
from datetime import date
from matplotlib.backends.backend_pdf import PdfPages
import os
import numpy as np


def dominance_reversal(genotype_condensed, relative_fitness):
    genotype_condensed_reversed = []
    reverse_indices = []
    relative_fitness_reversed = []
    n = len(genotype_condensed[0])
    # create reversed version
    # what is how reversed:
    # 1 -> 1
    # 0 -> 2
    # 2 -> 0
    for genotype in genotype_condensed:
        genotype_reversed = []
        for i in range(n):
            if genotype[i] == 0:
                genotype_reversed.append(2)
            elif genotype[i] == 1:
                genotype_reversed.append(1)
            elif genotype[i] == 2:
                genotype_reversed.append(0)
        genotype_condensed_reversed.append(genotype_reversed)

    # get indices to create reversed genotype vector upon
    for genotype in genotype_condensed_reversed:
        reverse_indices.append(genotype_condensed.index(genotype))

    # reversed fitness vector with before received indices
    for index in reverse_indices:
        relative_fitness_reversed.append(relative_fitness[index])
        # print(str(relative_fitness[index]) + " vs. " + str(buffer * relative_fitness[index]))

    print(genotype_condensed_reversed)

    return relative_fitness_reversed


def main():
    t = 300  # standard: 300

    mo = [line.strip() for line in open("enzyme_dominance_operationalFile.txt", "r")]
    n = int(mo[0])
    s = float(mo[4])

    genotype_condensed = []
    relative_fitness = []
    allele_frequencies = []
    frequency_change = []
    control_strings = []
    control_frequency = []

    symmetry = "y"  # (input("Symmetrical dominance reversal? (enter y (default) or n): ") or "y")
    season = 0.05  # float(input("Enter percentage a season has compared to time (default is 0.05): ") or "0.05")
    season_duration = season * t
    viability_method = "absolute"  # "slope_last", "slope_all", "absolute", "absolute_log"

    for i in range(2 * n):
        frequency_change.append([])

    # get information from documentation files
    with open("product_results.txt", "r") as file:
        product_results = json.load(file)

    with open("genotype_vector.txt", "r") as file:
        genotype_vector = json.load(file)

    # calculate relative fitness vector as proportional to product slope from other program
    # fitness value deliberately left out to single out viability effects
    # slope_last --> slope of last step
    length = len(product_results)
    if viability_method == "slope_last":
        for i in range(length):
            last_step = product_results[i][n - 1]
            lenProduct = len(last_step)
            # if substrate flux is present calculate slope
            if s > 0:
                # get large enough slope
                index_slope = int(lenProduct * 0.2)
                viability = (last_step[lenProduct - 1] - last_step[lenProduct - 1 - index_slope]) / index_slope
            else:
                viability = last_step[lenProduct - 1]
            relative_fitness.append(viability)
    # slope_all --> combination of slopes of all steps
    elif viability_method == "slope_all":
        for i in range(length):
            viability = 0
            for j in range(n):
                step = product_results[i][j]
                lenProduct = len(step)
                if s > 0:
                    index_slope = int(lenProduct * 0.2)
                    viability += (step[lenProduct - 1] - step[lenProduct - 1 - index_slope]) / index_slope
                else:
                    viability = step[lenProduct - 1]
            relative_fitness.append(viability)
    # sum of product concentrations of last time point for each genotype
    elif viability_method == "absolute":
        for i in range(length):
            viability = 0
            # check on sum of products by each step
            """for j in range(n):
                absolute_product = product_results[i][j]
                lenProduct = len(absolute_product)
                viability += absolute_product[lenProduct - 1]  # TODO: maybe check absolute value for ONLY last step
            relative_fitness.append(viability)"""
            absolute_product = product_results[i][1]
            lenProduct = len(absolute_product)
            viability = absolute_product[lenProduct - 1]
            relative_fitness.append(viability)
    elif viability_method == "absolute_log":
        for i in range(length):
            viability = 0
            for j in range(n):
                absolute_product = product_results[i][j]
                lenProduct = len(absolute_product)
                viability += log(absolute_product[lenProduct - 1])
            relative_fitness.append(viability)
    print(relative_fitness)

    # initial states for allele frequencies
    init_frequency = 0.5
    for i in range(n):
        allele_frequencies.append(init_frequency)

    # calculate condensed version of genotype
    # dominant allele -> 0
    # recessive allele -> 1
    # condensed genotype for a step is sum of two alleles:
    # heterozygous dominant = 0 + 1 = 1
    # homozygous dominant = 0 + 0 = 0
    # homozygous recessive = 1 + 1 = 2
    for genotype in genotype_vector:
        step_counter = 0
        genoSum_step = []
        genoSum = 0
        for i in range(2 * n):
            genoSum += int(genotype[i])
            step_counter += 1
            if step_counter % 2 == 0:
                genoSum_step.append(genoSum)
                genoSum = 0
        genotype_condensed.append(genoSum_step)
    print(genotype_condensed)

    # get reversed version of relative fitness vector
    relative_fitness_reversed = dominance_reversal(genotype_condensed, relative_fitness)

    fitness_vector = relative_fitness
    season_counter = 0

    standard_deviation = 0.05

    # calculate generations
    for i in range(t):
        probability_genotype = []
        denominator = 0
        string_denominator = ""

        # probabilities of genotypes
        for j in range(3 ** n):
            genotype = genotype_vector[j]
            probability = 1
            test_genotype = ""
            test_frequency = ""
            # loop over enzymes to get locus information
            for k in range(2 * n):
                step_counter = 0
                allele = genotype[k]
                frequency = allele_frequencies[step_counter]
                # calculated with variation of Hardy-Weinberg laws
                if allele == "0":
                    probability *= frequency
                    test_frequency += allele + " (" + str(frequency) + ")"
                    if k < (2 * n) - 1:
                        test_frequency += " * "
                elif allele == "1":
                    probability *= (1 - frequency)
                    test_frequency += allele + " (1 - " + str(frequency) + ")"
                    if allele != genotype[k - 1] and k % 2 != 0:
                        probability *= 2
                        test_frequency += " * 2"
                    if k < (2 * n) - 1:
                        test_frequency += " * "
                test_genotype += allele
                if (k + 1) % 2 == 0:
                    step_counter += 1
            test_frequency += " = " + str(probability)
            control_frequency.append(test_frequency)
            probability_genotype.append(probability)
        control_frequency.append(str(sum(probability_genotype)))

        # denominator
        for j in range(3 ** n):
            denominator += probability_genotype[j] * fitness_vector[j]
            string_denominator += "p_" + str(j) + " * w_" + str(j)
            if j < (3 ** n) - 1:
                string_denominator += " + "

        # calculate numerators for each step
        o = n - 1
        limit = (3 ** n) - 3 ** (n - 1)
        allele_counter = 0
        for j in range(n):
            numerator = 0
            string_numerator = ""
            index = -1
            counter = 1
            for k in range(limit):
                if k % (2 * 3 ** o) == 0 and k != 0:
                    index += 3 ** o + 1
                else:
                    index += 1
                # combination of probability of a genotype and its viability/fitness
                expr = probability_genotype[index] * fitness_vector[index]
                string_expr = "x_" + str(index) + " * w_" + str(index)
                if counter == 2:
                    numerator += expr
                    string_numerator += string_expr
                    if k < limit - 1:
                        string_numerator += " + "
                else:
                    numerator += 0.5 * expr
                    string_numerator += "0.5 * " + string_expr
                    if k < limit - 1:
                        string_numerator += " + "
                if (k + 1) % 3 ** o == 0:
                    counter += 1
                    if counter == 3:
                        counter = 1
            # frequency of the dominant allele
            frequency_dominant = numerator / denominator
            # frequency_dominant = round(frequency_dominant, 3)  # TODO: rounding is just for testing purposes!
            control_strings.append("step_" + str(j) + ": " + string_numerator + " / " + string_denominator)
            allele_frequencies[j] = frequency_dominant
            frequency_change[allele_counter].append(frequency_dominant)
            allele_counter += 1
            # frequency of two alleles of a locus adds to 1
            # frequency_recessive = round(1 - frequency_dominant, 3)
            frequency_change[allele_counter].append(1 - frequency_dominant)
            allele_counter += 1
            o -= 1
        # simulate dominance reversals with seasons
        # with reduced symmetry in dominance reversal
        if symmetry == "n":
            if (i + 1) % int(season_duration) == 0:
                if season_counter == 0:
                    # random_factor = np.random.normal(loc=1)
                    # print(str(random_factor) + ", type=" + str(type(random_factor)))
                    fitness_vector = [abs(np.random.normal(loc=1, scale=standard_deviation)) * x for x in relative_fitness_reversed]
                    # print(fitness_vector)
                    season_counter += 1
                else:
                    fitness_vector = relative_fitness
                    season_counter = 0
                # print(str((i + 1) / int(season_duration)) + ": " + str(fitness_vector))
        # fully symmetrical dominance reversal
        else:
            if (i + 1) % int(season_duration) == 0:
                if season_counter == 0:
                    fitness_vector = relative_fitness_reversed
                    season_counter += 1
                else:
                    fitness_vector = relative_fitness
                    season_counter = 0
        # print(fitness_vector)
        # fitness_vector = [round(x, 3) for x in fitness_vector]
        # print(fitness_vector)
        """else:
            if (i + 1) % int(season_duration * 0.9) == 0:
                if season_counter == 0:
                    fitness_vector = relative_fitness_reversed
                    season_counter += 1
                else:
                    fitness_vector = relative_fitness
                    season_counter = 0"""
            # print(fitness_vector)

    with open("control_strings.txt", "w") as file:
        for i in range(3 ** n):
            file.write(control_strings[i] + "\n\n")

    with open("control_frequencies.txt", "w") as file:
        for i in range(3 ** n + 1):
            file.write(control_frequency[i] + "\n\n")

    txt = "dominance_reversal_symmetry=" + str(symmetry) + ", season_duration=" + str(int(season_duration)) + ", standard_deviation=" + str(standard_deviation)

    # print(str(len(frequency_change)) + ": " + str(frequency_change))
    with open("plot_data_alleleFrequency.txt", "a") as file:
        file.write(str(mo) + "\n")
        print(mo)
        file.write(txt + "\n")
        runner = []
        # get frequency peak of last quarter of first enzyme of each step
        factor = int(3/4 * len(frequency_change[0]))
        for i in range(len(frequency_change)):
            if i % 2 == 0:
                allele_frequency_change = frequency_change[i][factor:]
                # print(allele_frequency_change)
                max_change = max(allele_frequency_change)
                # print(str(len(allele_frequency_change)) + ", " + str(max_change))
                runner.append(max_change)
        file.write(str(runner) + "\n")
        file.write("------\n")

    with open("parameter_change.txt", "r") as file:
        get_last_element = ""
        for line in file:
            get_last_element = line
        print(get_last_element)

    txt = "Entered information:\n" + str(mo) + txt
    # print(txt)

    # plots
    today = date.today()
    workdir = os.getcwd()

    if not os.path.isdir(workdir + "\\plots\\natural_selection"):
        os.mkdir(workdir + "\\plots\\natural_selection")

    with open("frequency_change.txt", "w") as file:
        for frequency in frequency_change:
            file.write(str(frequency) + "\n\n")

    fileExists = True
    counter = 0
    while fileExists:
        path = workdir + "\\plots\\natural_selection\\natural_selection_results_" + str(today) + "_" + str(n) + "_" + str(counter) + ".pdf"
        fileExists = os.path.isfile(path)
        if fileExists:
            counter += 1

    # generate plots for allele frequency for individual steps
    with PdfPages(path) as export_pdf:

        # put in information on experiment
        firstPage = plt.figure(figsize=(11.69, 8.27))
        firstPage.clf()
        firstPage.text(0.5, 0.5, txt, transform=firstPage.transFigure, size=8, ha="center", va="bottom")
        export_pdf.savefig()
        plt.close()

        counter = 0
        step_counter = 1
        for i in range(2 * n):
            if counter == 0:
                plt.plot(frequency_change[i], label="dominant allele")
            else:
                plt.plot(frequency_change[i], label="recessive allele")
            counter += 1
            if (i + 1) % 2 == 0:
                counter = 0
                plt.legend(shadow=True)
                plt.title("Natural Selection in Step " + str(step_counter))
                plt.grid()
                # plt.show()
                export_pdf.savefig()
                plt.close()
                step_counter += 1


if __name__ == "__main__":
    start_time = time.time()
    main()
    runtime = "--- %s seconds ---" % (time.time() - start_time)
    print(runtime)
    with open("runtime.txt", "a") as file:
        file.write(runtime)
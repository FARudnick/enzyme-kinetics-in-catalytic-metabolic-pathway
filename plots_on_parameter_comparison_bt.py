import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import os
from datetime import date
import json
from shutil import copyfile
from mpl_toolkits import mplot3d


def main():
    # n = mo[0]
    genotype = []
    get_new_data = (input("Copy new data from other programs (1=False, 0=True is standard)?") or "0")
    # print(get_new_data)
    what_parameter = ""
    changed_enzyme = ""
    what_enzyme = ""
    plot_data_products = []
    plot_data_dominance = []
    plot_data_alleleFrequency = []
    population_experiment_information = ""
    x_axis = []
    y_axis_products = []
    y_axis_dominance = []
    y_axis_alleleFrequency = []
    file_path = "PATH_TO_INFO_FILES"
    dest_path = "PATH_TO_WORKDIR_OF_THIS_PROGRAM"

    # get correct files into working directory
    if get_new_data == "0":
        copyfile(file_path + "what_parameter.txt", dest_path + "what_parameter.txt")
        with open("what_parameter.txt", "r") as file:
            what_parameter = file.readline().strip()
            changed_enzyme = file.readline()
        copyfile(file_path + "parameter_change.txt", dest_path + "parameter_change.txt")
        if what_parameter != "sflux":
            copyfile(file_path + "parameter_temp.txt", dest_path + "parameter_temp.txt")
        copyfile(file_path + "enzyme_dominance_operationalFile.txt", dest_path + "enzyme_dominance_operationalFile.txt")
        copyfile(file_path + "plot_data_alleleFrequency.txt", dest_path + "plot_data_alleleFrequency.txt")
        copyfile(file_path + "plot_data_dominance.txt", dest_path + "plot_data_dominance.txt")
        copyfile(file_path + "plot_data_products.txt", dest_path + "plot_data_products.txt")
        copyfile(file_path + "product_results.txt", dest_path + "product_results.txt")
    elif get_new_data == "1":
        with open("what_parameter.txt", "r") as file:
            what_parameter = file.readline().strip()
            changed_enzyme = file.readline()

    print(what_parameter + " -> " + changed_enzyme)
    mo = [line.strip() for line in open("enzyme_dominance_operationalFile.txt", "r")]
    print(mo)

    # loop over product information to generate matrix for plot
    with open("plot_data_products.txt", "r") as file:
        runner = []
        for line in file:
            if not runner:
                line = line.replace("\n", "")
                line = line.replace("[", "")
                line = line.replace("]", "")
                line = line.replace("'", "")
                # print(line)
                new_mo = line.split(", ")
                # print(new_mo)
                runner.append(new_mo)
            elif line == "------\n":
                # del runner[-1]  # remove the "------\n" element
                plot_data_products.append(runner)
                runner = []
            else:
                line = line.replace("\n", "")
                line = line.replace("[", "")
                line = line.replace("]", "")
                line = line.replace("'", "")
                new_line = line.split(", ")
                new_line = [float(x) for x in new_line]
                runner.append(new_line)

    # loop over dominance information
    with open("plot_data_dominance.txt", "r") as file:
        runner = []
        for line in file:
            if not runner:
                line = line.replace("\n", "")
                line = line.replace("[", "")
                line = line.replace("]", "")
                line = line.replace("'", "")
                # print(line)
                new_mo = line.split(", ")
                # print(new_mo)
                runner.append(new_mo)
            elif line == "------\n":
                # del runner[-1]  # remove the "------\n" element
                plot_data_dominance.append(runner)
                runner = []
            else:
                line = line.replace("\n", "")
                line = line.replace("[", "")
                line = line.replace("]", "")
                line = line.replace("'", "")
                new_line = line.split(", ")
                new_line = [float(x) for x in new_line]
                runner.append(new_line)
        # print(plot_data_products)
        # print(plot_data_dominance)

    with open("plot_data_alleleFrequency.txt", "r") as file:
        runner = []
        title = 0
        info_counter = 0
        for line in file:
            if not runner:
                line = line.replace("\n", "")
                line = line.replace("[", "")
                line = line.replace("]", "")
                line = line.replace("'", "")
                # print(line)
                new_mo = line.split(", ")
                # print(new_mo)
                runner.append(new_mo)
                title = 1
            elif line == "------\n":
                plot_data_alleleFrequency.append(runner)
                runner = []
            elif title == 1:
                if info_counter != 1:
                    line = line.replace("\n", "")
                    line = line.replace("[", "")
                    line = line.replace("]", "")
                    line = line.replace("'", "")
                    # print(line)
                    new_extendedInfo = line.split(", ")
                    population_experiment_information = new_extendedInfo
                    title = 0
                    info_counter = 1
                else:
                    title = 0
                    pass
            else:
                line = line.replace("\n", "")
                line = line.replace("[", "")
                line = line.replace("]", "")
                line = line.replace("'", "")
                new_line = line.split(", ")
                new_line = [float(x) for x in new_line]
                # print(new_line)
                runner.append(new_line)
    # for line in plot_data_alleleFrequency:
    #     print(str(line))

    with open("genotype_vector.txt", "r") as file:
        genotype = json.load(file)
    # print(str(genotype))

    # get x-axis
    # I'll be hard coding here: in the first case the x-axis will be different substrate fluxes
    """for element in plot_data_products:
        # print(element[0][4])
        x_axis.append(float(element[0][4]))"""
        # print(float(element[0][4]))
    # hard-coding for parameter change
    """parameter_change = [line.strip() for line in open("parameter_change.txt", "r")]
    # print(parameter_change)
    # del parameter_change[0]
    x_axis = [float(x) for x in parameter_change]"""
    # print(parameter_change)
    """for element in plot_data_alleleFrequency:
        # print(element[0][4])
        x_axis.append(float(element[0][4]))"""
    if what_parameter == "sflux":
        for element in plot_data_products:
            # print(float(element[0][4]))
            x_axis.append(float(element[0][4]))
    else:
        parameter_change = [line.strip() for line in open("parameter_change.txt", "r")]
        x_axis = [float(x) for x in parameter_change]
    # print(x_axis)

    # os._exit(0)

    # element = plot_data_products[0]
    element = plot_data_alleleFrequency[0]
    n = int(element[0][0])
    # print(n)

    length = len(x_axis)
    # print(x_axis)
    # get y-axis elements for products
    for k in range(3 ** n):
        runner_step = []
        for i in range(n):
            runner = []
            for j in range(length):
                element = plot_data_products[j][k + 1]
                runner.append(element[i])
            runner_step.append(runner)
        y_axis_products.append(runner_step)
    # print(y_axis_products)

    # print(len(plot_data_dominance))
    # print(plot_data_dominance)
    # get y-axis elements for dominance
    for i in range(n * 3**(n - 1)):
        for j in range(2):
            runner_step = []
            for k in range(length):
                element = plot_data_dominance[k][i + 1]
                runner_step.append(element[j])
            y_axis_dominance.append(runner_step)

    # for i in y_axis_dominance:
    #    print(i)

    # print(y_axis_dominance)
    # get y-axis for allele frequency data
    # hard coded for two metabolic steps
    # print(plot_data_alleleFrequency)
    step_one = []
    step_two = []
    for element in plot_data_alleleFrequency:
        step_one.append(element[1][0])
        step_two.append(element[1][1])
    y_axis_alleleFrequency.append(step_one)
    y_axis_alleleFrequency.append(step_two)
    # print(y_axis_alleleFrequency)

    today = date.today()
    workdir = os.getcwd()
    pdf_name = "\\paramter_comparison_plots_" + str(today) + ".pdf"
    path = workdir + pdf_name
    enzyme1_concentration = mo[5]
    enzyme2_concentration = mo[6]
    substrate_flux = mo[4].replace(".", "")
    path_svg = workdir + "\\out\\" + what_parameter + "_" + enzyme1_concentration + "-" + enzyme2_concentration + "_" + substrate_flux + "_" + str(today) + "_"

    # hard-coded for two steps
    genotype_other_step = ["01", "00", "11"]

    description_size = 14
    title_size = 5

    # what_parameter = input("What parameter was changed? ")
    if what_parameter != "sflux":
        if changed_enzyme == "0":
            what_enzyme = "first"
        elif changed_enzyme == "1":
            what_enzyme = "second"

    # create plots for products
    with PdfPages(path) as export_pdf:

        # put in information on experiment
        firstPage = plt.figure(figsize=(11.69, 8.27))
        firstPage.clf()
        if what_parameter == "sflux":
            txt = " enzyme concentrations: \n| "
            for i in range(2 * n):
                if (i + 1) % 2 == 0:
                    txt += mo[5 + i] + " | "
                else:
                    txt += mo[5 + i] + ", "
        else:
            txt = "Entered information: \n " + str(mo)
        txt += "\nAdditional Information:\n" + str(population_experiment_information) + "\n viability_method=absolute"
        firstPage.text(0.5, 0.5, txt, transform=firstPage.transFigure, size=12, ha="center", va="bottom")
        export_pdf.savefig()
        plt.savefig(path_svg + "info.svg")
        plt.close()

        # fig = plt.figure()
        # ax = plt.axes(projection="3d")

        # horizontal
        x_line = x_axis
        # depth
        y_line = []
        for i in range(len(x_axis)):
            y_line.append(0)

        for i in range(n):
            ax = plt.axes(projection="3d")
            for j in range(3 ** n):
                for k in range(len(x_axis)):
                    y_line[k] = j
                # vertical
                z_line = y_axis_products[j][i]
                ax.plot3D(x_line, y_line, z_line, label=str(genotype[j]))
            plt.legend(shadow=True, title="genotype", loc="upper left")
            ax.set_xlabel(str(what_parameter), size=description_size)
            ax.set_ylabel("genotype index", size=description_size)
            ax.set_zlabel("product concentration", size=description_size)
            """if what_parameter == "sflux":
                ax.set_title("Substrate flux change (step " + str(i + 1) + ")")
            else:
                ax.set_title("Product concentration at fixed time point \n vs. different " + str(what_parameter) + " values of " + str(what_enzyme) + " enzyme (step " + str(i + 1) + ")", size=title_size)"""
            y_ticks = [x for x in range(3 ** n)]
            # print(y_ticks)
            ax.set_yticks(ticks=y_ticks)
            # plt.show()
            plt.tight_layout()
            plt.tick_params(axis="x", labelsize=description_size)
            plt.tick_params(axis="y", labelsize=description_size)
            plt.tick_params(axis="z", labelsize=description_size)
            plt.xticks(rotation=45)
            export_pdf.savefig(bbox_inches="tight")
            plt.savefig(path_svg + "products_step" + str(i + 1) + ".svg")
            plt.close()

        geno_iterator = 0

        # create plots for dominance
        ax = plt.axes(projection="3d")
        step_counter = n  # reverse counted
        for i in range((n * 3**(n - 1)) * 2):
            if i < 6:
                if (i + 1) % 2 == 0:
                    # ax.plot3D(x_line, y_line, y_axis_dominance[i], label=" -> rec(" + str(step_counter) + ":" + genotype_other_step[geno_iterator] + ")")
                    # plt.plot(x_axis, y_axis_dominance[i], label="rec(" + genotype_other_step[geno_iterator] + "+" + str(step_counter) + ")", alpha=0.5)
                    geno_iterator += 1
                else:
                    for j in range(len(x_axis)):
                        y_line[j] = int(i)
                    z_line = y_axis_dominance[i]
                    # print(z_line)
                    ax.plot3D(x_line, y_line, z_line, label="e1(" + str(step_counter) + ":" + genotype_other_step[geno_iterator] + ")")
                    # plt.plot(x_axis, y_axis_dominance[i], label="dom(" + genotype_other_step[geno_iterator] + "+" + str(step_counter) + ")", alpha=0.5)
            else:
                if i == 6:
                    plt.legend(shadow=True, loc="upper left")
                    # plt.grid()
                    # plt.xlabel("k_f")
                    # plt.xlabel("substrate flux")
                    # plt.ylabel("dominance value")
                    # plt.title("Dominance value at fixed time point (last) \n vs. different k_f values of dominant enzyme of first step \n (step 1)")
                    # plt.title("Dominance value at fixed time point (last) \n vs. different substrate flux values \n step 1")
                    ax.set_xlabel(str(what_parameter), size=description_size)
                    ax.set_ylabel("legend index", size=description_size)
                    ax.set_zlabel("dominance value", size=description_size)
                    """if what_parameter == "sflux":
                        ax.set_title("Substrate flux change (step 1)")
                    else:
                        ax.set_title("Dominance value at fixed time point (last) \n vs. different " + str(what_parameter) + " values of " + str(what_enzyme) + " enzyme (step 1)", size=title_size)"""
                    y_ticks = [x for x in range(5)]
                    ax.set_yticks(ticks=y_ticks)
                    # plt.show()
                    plt.tight_layout()
                    plt.tick_params(axis="x", labelsize=description_size)
                    plt.tick_params(axis="y", labelsize=description_size)
                    plt.tick_params(axis="z", labelsize=description_size)
                    plt.xticks(rotation=45)
                    export_pdf.savefig(bbox_inches="tight")
                    plt.savefig(path_svg + "dominance_step1.svg")
                    plt.close()
                    geno_iterator = 0
                    step_counter -= 1
                    ax = plt.axes(projection="3d")
                if (i + 1) % 2 == 0:
                    # ax.plot3D(x_line, y_line, y_axis_dominance[i], label=" -> rec(" + str(step_counter) + ":" + genotype_other_step[geno_iterator] + ")")
                    # plt.plot(x_axis, y_axis_dominance[i], label="rec(" + genotype_other_step[geno_iterator] + "+" + str(step_counter) + ")", alpha=0.5)
                    geno_iterator += 1
                else:
                    for j in range(len(x_axis)):
                        y_line[j] = int(i)
                    z_line = y_axis_dominance[i]
                    # print(z_line)
                    ax.plot3D(x_line, y_line, z_line, label="e1(" + str(step_counter) + ":" + genotype_other_step[geno_iterator] + ")")
                    # plt.plot(x_axis, y_axis_dominance[i], label="dom(" + genotype_other_step[geno_iterator] + "+" + str(step_counter) + ")", alpha=0.5)
        plt.legend(shadow=True, loc="upper left")
        # plt.grid()
        # plt.xlabel("k_f")
        # plt.xlabel("substrate flux")
        # plt.ylabel("dominance value")
        # plt.title("Dominance value at fixed time point (last) \n vs. different k_f values of dominant enzyme of first step \n (step 2)")
        # plt.title("Dominance value at fixed time point (last) \n vs. different substrate flux values \n (step 2)")
        ax.set_xlabel(str(what_parameter), size=description_size)
        ax.set_ylabel("legend index", size=description_size)
        ax.set_zlabel("dominance value", size=description_size)
        """if what_parameter == "sflux":
            ax.set_title("Substrate flux change (step 2)")
        else:
            ax.set_title("Dominance value at fixed time point (last) \n vs. different "+ str(what_parameter) + " values of" + str(what_enzyme) + "enzyme (step 2)", size=title_size)"""
        y_ticks = [x for x in range(6, 11)]
        ax.set_yticks(ticks=y_ticks)
        # plt.show()
        plt.tick_params(axis="x", labelsize=description_size)
        plt.tick_params(axis="y", labelsize=description_size)
        plt.tick_params(axis="z", labelsize=description_size)
        plt.tight_layout()
        plt.xticks(rotation=45)
        export_pdf.savefig(bbox_inches="tight")
        plt.savefig(path_svg + "dominance_step2.svg")
        plt.close()

        # plot allele frequency
        """ax = plt.axes(projection="3d")
        # depth
        # y_counter = 1
        y_line = []
        for i in range(len(x_axis)):
            y_line.append(0)
        z_line = y_axis_alleleFrequency
        for i in range(2):
            for j in range(len(x_axis)):
                y_line[j] = i
            # y_counter -= 1
            ax.plot3D(x_axis, y_line, z_line[i], label="step " + str(i + 1))
        plt.legend(shadow=True, title="first enzyme of ", loc="upper left")
        ax.set_xlabel("k_f of first enzyme of first step")
        ax.set_ylabel("legend index")
        ax.set_zlabel("allele frequency")"""

        # plot allele frequency
        for i in range(2):
            plt.plot(x_axis, y_axis_alleleFrequency[i], label="step " + str(i + 1))
        plt.legend(shadow=True, title="first enzyme of ")
        if what_parameter == "sflux":
            plt.xlabel("sflux")
        else:
            plt.xlabel(str(what_parameter) + " value of " + str(what_enzyme) + " enzyme in step 1", size=description_size)
        plt.ylabel("allele frequency", size=description_size)
        plt.grid()
        plt.tight_layout()
        plt.tick_params(axis="x", labelsize=description_size)
        plt.tick_params(axis="y", labelsize=description_size)
        plt.xticks(rotation=315)
        export_pdf.savefig(bbox_inches="tight")
        plt.savefig(path_svg + "naturalSelection.svg")
        plt.close()

    # delete files in full_program folder --> fixed path, be careful!
    if get_new_data == "0":
        os.remove(file_path + "parameter_change.txt")
        if what_parameter != "sflux":
            os.remove(file_path + "parameter_temp.txt")
        os.remove(file_path + "plot_data_alleleFrequency.txt")
        os.remove(file_path + "plot_data_dominance.txt")
        os.remove(file_path + "plot_data_products.txt")
        os.remove(file_path + "product_results.txt")
        os.remove(file_path + "what_parameter.txt")


if __name__ == '__main__':
    main()
